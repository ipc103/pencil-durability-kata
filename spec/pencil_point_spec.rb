require_relative './spec_helper'

describe PencilPoint do
  let(:pencil_point) { PencilPoint.new(durability: 50) }
  it_behaves_like 'Durable'

  context 'writing' do
    context 'to a paper with text' do
      let(:paper) { "She sells sea shells" }

      it 'can add text to the end of the paper' do
        pencil_point.write(paper, " down by the sea shore")
        expect(paper).to eq("She sells sea shells down by the sea shore")
      end

      it 'appends different text to the paper' do
        pencil_point.write(paper, " high on the mountain top")
        expect(paper).to eq("She sells sea shells high on the mountain top")
      end
    end

    context 'to a blank page' do
      let(:paper) { '' }

      it 'appends to a blank sheet of paper' do
        text = 'She sells sea shells down by the sea shore'
        pencil_point.write(paper, text)
        expect(paper).to eq(text)
      end
    end

    context 'when dull' do
      let(:dull_pencil_point) { PencilPoint.new(durability: 0) }
      it 'adds spaces to the paper if the pencil_point is dull' do
        paper = "She sells sea shells"
        dull_pencil_point.write(paper, 'abcd')
        expect(paper).to eq("She sells sea shells    ")
      end

      it 'adds a space for the length of the text' do
        paper = "She sells sea shells"
        dull_pencil_point.write(paper, "12345678")
        expect(paper).to eq("She sells sea shells        ")
      end
    end

    context 'with a low point' do
      it 'can write some characters' do
        pencil_point = PencilPoint.new(durability: 4)
        paper = ""
        pencil_point.write(paper, 'Text')
        expect(paper).to eq('Tex ')
      end
    end
  end

  context 'sharpening' do
    let(:pencil_point) { PencilPoint.new(durability: 7)}

    it 'restores the initial point durability' do
      pencil_point.write('', '1234567')
      expect(pencil_point.durability).to eq(0)
      pencil_point.sharpen!
      expect(pencil_point.durability).to eq(7)
    end
  end
end
