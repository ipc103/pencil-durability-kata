require 'bundler'
Bundler.require

require_relative '../lib/concerns/durable'
require_relative '../lib/editor'
require_relative '../lib/eraser'
require_relative '../lib/pencil_point'
require_relative '../lib/pencil'
