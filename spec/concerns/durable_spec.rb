require_relative '../spec_helper'

RSpec.shared_examples 'Durable' do
  let(:durable_item) { described_class.new(durability: 30)}

  context 'durability' do
    it "can be set on initialization" do
      expect(durable_item.durability).to eq(30)
    end
  end

end
