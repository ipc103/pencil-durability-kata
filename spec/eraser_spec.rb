require_relative './spec_helper'

describe Eraser do
  let(:eraser) { Eraser.new(durability: 50) }
  it_behaves_like 'Durable'

  context 'erasing text from paper' do
    it 'degrades by a value of 1 for each non-whitespace character' do
      initial_durability = eraser.durability
      eraser.erase("Paper", "er")
      expect(eraser.durability).to eq(initial_durability - 2)
    end

    it "doesn't degrade when erasing whitespace" do
      initial_durability = eraser.durability
      eraser.erase("Some text", " text")
      expect(eraser.durability).to eq(initial_durability - 4)
    end

    it "can't go below zero" do
      eraser = Eraser.new(durability: 3)
      eraser.erase("12345", "12345")
      expect(eraser.durability).to eq(0)
    end
    
    it "doesn't change the paper if there is no match" do
      paper = "ABCD"
      eraser.erase(paper, "Zorp!")
      expect(paper).to eq("ABCD")
    end

    it 'replaces the last instance of text on paper with empty spaces' do
      paper =  "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
      eraser.erase(paper, 'chuck')
      expect(paper).to eq("How much wood would a woodchuck chuck if a woodchuck could       wood?")
    end

    it "doesn't erase if it is worn out" do
      worn_out = Eraser.new(durability: 0)
      paper = "Some text"
      worn_out.erase(paper, "text")
      expect(paper).to eq("Some text")
    end

    it "erasers some characters if the eraser is almost worn out" do
      almost_worn = Eraser.new(durability: 3)
      paper = "Buffalo Bill"
      almost_worn.erase(paper, "Bill")
      expect(paper).to eq("Buffalo B   ")
    end
  end
end
