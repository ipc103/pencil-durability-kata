require_relative './spec_helper'

describe 'Editor' do
  context 'editing paper' do
    context 'without any whitespace' do
      let(:paper) { "An apple a day" }

      it "doesn't change a paper that hasn't been erased yet" do
        editor = Editor.new(paper, "artichoke")
        editor.edit
        expect(paper).to eq("An apple a day")
      end
    end

    context 'with whitespace' do
      let(:paper) { "An       a day keeps the doctor away" }

      it "replaces chunks of whitespace with the new text" do
        Editor.new(paper, "apple").edit
        expect(paper).to eq("An apple a day keeps the doctor away")
      end

      it "replaces longer whitespace with a shorter word" do
        Editor.new(paper, "ant").edit
        expect(paper).to eq("An ant a day keeps the doctor away")
      end

      it "represents collisions with the @ character" do
        Editor.new(paper, 'artichoke').edit
        expect(paper).to eq("An artich@k@ay keeps the doctor away")
      end

      it "adds characters to the end of the paper if needed" do
        paper = "An       a"
        Editor.new(paper, "everlasting gobstopper").edit
        expect(paper).to eq("An everla@ting gobstopper")
      end
    end
  end

end
