require_relative './spec_helper'

describe 'Pencil' do
  context 'writing' do
    let(:pencil) { Pencil.new }

    context 'to a paper with text' do
      let(:paper) { "She sells sea shells" }

      it 'can add text to the end of the paper' do
        pencil.write(paper, " down by the sea shore")
        expect(paper).to eq("She sells sea shells down by the sea shore")
      end

      it 'appends different text to the paper' do
        pencil.write(paper, " high on the mountain top")
        expect(paper).to eq("She sells sea shells high on the mountain top")
      end
    end

    context 'to a blank page' do
      let(:paper) { '' }

      it 'appends to a blank sheet of paper' do
        text = 'She sells sea shells down by the sea shore'
        pencil.write(paper, text)
        expect(paper).to eq(text)
      end
    end

    context 'when dull' do
      it 'adds spaces to the paper if the pencil is dull' do
        pencil = Pencil.new(point_durability: 0)
        paper = "She sells sea shells"
        pencil.write(paper, 'abcd')
        expect(paper).to eq("She sells sea shells    ")
      end

      it 'adds a space for the length of the text' do
        pencil = Pencil.new(point_durability: 0)
        paper = "She sells sea shells"
        pencil.write(paper, "12345678")
        expect(paper).to eq("She sells sea shells        ")
      end
    end

    context 'with a low point' do
      it 'can write some characters' do
        pencil = Pencil.new(point_durability: 4)
        paper = ""
        pencil.write(paper, 'Text')
        expect(paper).to eq('Tex ')
      end
    end
  end

  context 'point durability' do
    it 'can be created with a point durability' do
      pencil = Pencil.new(point_durability: 10)
      expect(pencil.point_durability).to eq(10)
    end

    it 'can have a higher point durability' do
      pencil = Pencil.new(point_durability: 500)
      expect(pencil.point_durability).to eq(500)
    end

    it 'has a default point value of 800' do
      pencil = Pencil.new
      expect(pencil.point_durability).to eq(800)
    end

    it 'lowers when writing' do
      pencil = Pencil.new
      pencil.write("", "1234")
      expect(pencil.point_durability).to eq(796)
    end

    it 'lowers based on the length of the text' do
      pencil = Pencil.new(point_durability: 300)
      pencil.write("", "123456789")
      expect(pencil.point_durability).to eq(291)
    end

    it "doesn't degrade when writing spaces or newlines" do
      pencil = Pencil.new(point_durability: 300)
      pencil.write("", "  \n  ")
      expect(pencil.point_durability).to eq(300)
    end

    it "can't go below zero" do
      pencil = Pencil.new(point_durability: 1)
      pencil.write("", "123456")
      expect(pencil.point_durability).to eq(0)
    end

    it "degrades double for capital letters" do
      pencil = Pencil.new(point_durability: 10)
      pencil.write("", "FOUR")
      expect(pencil.point_durability).to eq(2)
    end
  end

  context 'length' do
    it 'can get set on initialization' do
      pencil = Pencil.new(length: 50)
      expect(pencil.length).to eq(50)
    end

    it 'defaults to ten' do
      pencil = Pencil.new
      expect(pencil.length).to eq(10)
    end
  end

  context 'sharpening' do
    let(:pencil) { Pencil.new(point_durability: 7, length: 10)}

    it 'restores the initial point durability' do
      pencil.write('', '1234567')
      expect(pencil.point_durability).to eq(0)
      pencil.sharpen
      expect(pencil.point_durability).to eq(7)
    end

    it "lowers the pencil's length by one" do
      pencil.sharpen
      expect(pencil.length).to eq(9)
    end

    context 'with no remaining length' do
      it "can't sharpen the pencil" do
        pencil = Pencil.new(point_durability: 5, length: 0)
        pencil.write("", "12345")
        pencil.sharpen
        expect(pencil.point_durability).to eq(0)
      end
    end
  end

  context 'erasing text' do
    let(:pencil) { Pencil.new }
    it "doesn't change the paper if there is no match" do
      paper = "ABCD"
      pencil.erase(paper, "Zorp!")
      expect(paper).to eq("ABCD")
    end

    it 'replaces the last instance of text on paper with empty spaces' do
      paper =  "How much wood would a woodchuck chuck if a woodchuck could chuck wood?"
      pencil.erase(paper, 'chuck')
      expect(paper).to eq("How much wood would a woodchuck chuck if a woodchuck could       wood?")
    end

    it "doesn't erase if the eraser is worn out" do
      without_eraser = Pencil.new(eraser_durability: 0)
      paper = "Some text"
      without_eraser.erase(paper, "text")
      expect(paper).to eq("Some text")
    end

    it "erasers some characters if the eraser is almost worn out" do
      pencil = Pencil.new(eraser_durability: 3)
      paper = "Buffalo Bill"
      pencil.erase(paper, "Bill")
      expect(paper).to eq("Buffalo B   ")
    end
  end

  context 'eraser durability' do
    let(:pencil) { Pencil.new }
    it "can be set on initialization" do
      pencil = Pencil.new(eraser_durability: 50)
      expect(pencil.eraser_durability).to eq(50)
    end

    it "defaults to 100" do
      expect(pencil.eraser_durability).to eq(100)
    end

    it 'degrades by a value of 1 for each non-whitespace character' do
      initial_eraser_durability = pencil.eraser_durability
      pencil.erase("Paper", "er")
      expect(pencil.eraser_durability).to eq(initial_eraser_durability - 2)
    end

    it "doesn't degrade when erasing whitespace" do
      initial_eraser_durability = pencil.eraser_durability
      pencil.erase("Some text", " text")
      expect(pencil.eraser_durability).to eq(initial_eraser_durability - 4)
    end

    it "can't go below zero" do
      pencil = Pencil.new(eraser_durability: 3)
      pencil.erase("12345", "12345")
      expect(pencil.eraser_durability).to eq(0)
    end
  end

  context 'editing paper' do
    let(:pencil) { Pencil.new }

    context 'without any whitespace' do
      let(:paper) { "An apple a day" }

      it "doesn't change a paper that hasn't been erased yet" do
        pencil.edit(paper, "artichole")
        expect(paper).to eq("An apple a day")
      end
    end

    context 'with whitespace' do
      let(:paper) { "An       a day keeps the doctor away" }

      it "replaces chunks of whitespace with the new text" do
        pencil.edit(paper, "apple")
        expect(paper).to eq("An apple a day keeps the doctor away")
      end

      it "replaces longer whitespace with a shorter word" do
        pencil.edit(paper, "ant")
        expect(paper).to eq("An ant a day keeps the doctor away")
      end

      it "represents collisions with the @ character" do
        pencil.edit(paper, 'artichoke')
        expect(paper).to eq("An artich@k@ay keeps the doctor away")
      end

      it "adds characters to the end of the paper if needed" do
        paper = "An       a"
        pencil.edit(paper, "everlasting gobstopper")
        expect(paper).to eq("An everla@ting gobstopper")
      end
    end
  end
end
