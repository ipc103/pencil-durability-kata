class Editor
  attr_reader :paper, :text

  def initialize(paper, text)
    @paper = paper
    @text  = text
  end

  def edit
    return unless has_editable_space?

    if edits_fit_in_space?
      paper.sub!(editable_space, " #{text} ")
    else
      insert_text_with_collisions
    end
  end

  private

  def insert_text_with_collisions
    paper_index = paper.index(editable_space) + 1
    text_index = 0
    while text_index < text.length
      if character_is_blank?(paper_index)
        paper[paper_index] = text[text_index]
      else
        paper[paper_index] = collision_character
      end
      paper_index += 1
      text_index += 1
    end
  end

  def collision_character
    "@"
  end

  def character_is_blank?(index)
    paper[index] == " " || paper[index].nil?
  end

  def has_editable_space?
    !!editable_space_match
  end

  def editable_space_match
    @editable_space_match ||= paper.match(/ {3,}/)
  end

  def editable_space
    if has_editable_space?
      editable_space_match[0]
    end
  end

  def edits_fit_in_space?
    editable_space.length >= text.length + 2
  end
end
