module Durable
  attr_reader :durability
  
  def initialize(durability:)
    @durability = durability
  end

end
