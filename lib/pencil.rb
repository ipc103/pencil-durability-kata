class Pencil
  attr_reader :length

  DEFAULT_POINT_DURABILITY = 800
  DEFAULT_LENGTH = 10
  DEFAULT_ERASER_DURABILITY = 100

  def initialize(point_durability: DEFAULT_POINT_DURABILITY, length: 10, eraser_durability: DEFAULT_ERASER_DURABILITY)
    @length = length
    @point  = PencilPoint.new(durability: point_durability)
    @eraser = Eraser.new(durability: eraser_durability)
  end

  def write(paper, text)
    point.write(paper, text)
  end

  def sharpen
    return if length.zero?
    @length -= 1
    point.sharpen!
  end

  def erase(paper, text)
    eraser.erase(paper, text)
  end

  def edit(paper, text)
    Editor.new(paper, text).edit
  end

  def eraser_durability
    eraser.durability
  end

  def point_durability
    point.durability
  end

  private

  attr_reader :eraser, :point

end
