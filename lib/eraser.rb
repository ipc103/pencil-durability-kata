class Eraser
  include Durable

  def erase(paper, text)
    if paper.include?(text)
      reversed_text = text.reverse
      replacement_text = erasable(reversed_text)
      paper.reverse!.sub!(reversed_text, replacement_text)
      paper.reverse!
    end
  end

  private

  def erasable(text)
    erasable_characters(text).join("")
  end

  def erasable_characters(text)
    text.chars.map {|char| erasable_character(char) }
  end

  def erasable_character(char)
    if durability.zero?
      char
    else
      @durability -= 1 unless char == " "
      " "
    end
  end
end
