class PencilPoint
  include Durable

  def initialize(durability:)
    super(durability: durability)
    @initial_durability = durability
  end

  def write(paper, text)
    text.each_char { |char| write_character(paper, char) }
  end

  def sharpen!
    @durability = @initial_durability
  end

  private

  def write_character(paper, char)
    if can_write?(char)
      @durability -= degradation(char)
      paper << char
    else
      paper << " "
    end
  end

  def can_write?(char)
    durability >= degradation(char)
  end

  def degradation(char)
     char.scan(/\w/).length + char.scan(/[A-Z]/).length
  end
end
